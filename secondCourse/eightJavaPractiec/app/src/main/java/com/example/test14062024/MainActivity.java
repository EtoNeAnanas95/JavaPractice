package com.example.test14062024;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private Button One, Two, Three, Four, Five, Six, Seven, Eight, Nine, Zero;
    private Button Minus, Plus, Division, Multiply, Equal, Sqrt, Sqr, Percent, Clear, Delete;
    private TextView Res;
    private char Action;
    private double ResultValue = Double.NaN;
    private double Value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        setup();

        View.OnClickListener numbersClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                if (Res.getText().length() == 1 && Res.getText() == "0") Res.setText(null);
                Res.setText(Res.getText().toString() + button.getText().toString());
            }
        };

        One.setOnClickListener(numbersClickListener);
        Two.setOnClickListener(numbersClickListener);
        Three.setOnClickListener(numbersClickListener);
        Four.setOnClickListener(numbersClickListener);
        Five.setOnClickListener(numbersClickListener);
        Six.setOnClickListener(numbersClickListener);
        Seven.setOnClickListener(numbersClickListener);
        Eight.setOnClickListener(numbersClickListener);
        Nine.setOnClickListener(numbersClickListener);
        Zero.setOnClickListener(numbersClickListener);

        //////////////////////////////////////////////////////////

        View.OnClickListener mathClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Button button = (Button) view;
                if (Res.getText().length() != 0 && Character.isDigit(Res.getText().toString().charAt(Res.getText().toString().length() - 1)) || Res.getText().length() == 0) {
                    GoMath();
                    Action = button.getText().charAt(0);
                    Res.setText(String.valueOf(ResultValue) + Action);
                }
            }
        };

        Plus.setOnClickListener(mathClickListener);
        Minus.setOnClickListener(mathClickListener);
        Division.setOnClickListener(mathClickListener);
        Multiply.setOnClickListener(mathClickListener);
        Percent.setOnClickListener(mathClickListener);

        //////////////////////////////////////////////////////////

        Equal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Res.getText().length() != 0 && Character.isDigit(Res.getText().toString().charAt(Res.getText().toString().length() - 1))) {
                    GoMath();
                    Action = '=';
                    Res.setText(String.valueOf(ResultValue));
                }
            }
        });

        //////////////////////////////////////////////////////////

        View.OnClickListener MegaMathClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Button button = (Button) view;
                if (Res.getText().length() != 0 &&
                        Character.isDigit(Res.getText().toString().charAt(Res.getText().toString().length() - 1))
                        || Res.getText().length() == 0) {
                    GoMath();
                    Action = button.getText().charAt(0);
                    Res.setText(String.valueOf(ResultValue) + Action);
                    MegaGoMath();
                    Res.setText(String.valueOf(ResultValue));
                }
            }
        };

        Sqrt.setOnClickListener(MegaMathClickListener);
        Sqr.setOnClickListener(MegaMathClickListener);

        //////////////////////////////////////////////////////////

        Clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Res.setText("");
                ResultValue = Double.NaN;
                Res.setText("0");
            }
        });

        //////////////////////////////////////////////////////////

        Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Res.getText().length() != 0) Res.setText(((String) Res.getText()).substring(0, Res.getText().length() - 1));
            }
        });
    }

    private void setup() {
        One = findViewById(R.id.one);
        Two = findViewById(R.id.two);
        Three = findViewById(R.id.three);
        Four = findViewById(R.id.four);
        Five = findViewById(R.id.five);
        Six = findViewById(R.id.six);
        Seven = findViewById(R.id.seven);
        Eight = findViewById(R.id.eight);
        Nine = findViewById(R.id.nine);
        Zero = findViewById(R.id.zero);
        Minus = findViewById(R.id.minus);
        Plus = findViewById(R.id.plus);
        Equal = findViewById(R.id.equal);
        Delete = findViewById(R.id.delete);
        Division = findViewById(R.id.divide);
        Multiply = findViewById(R.id.multiply);
        Sqrt = findViewById(R.id.sqrt);
        Sqr = findViewById(R.id.sqr);
        Percent = findViewById(R.id.percent);
        Clear = findViewById(R.id.clear);
        Res = findViewById(R.id.result);
    }

    private void GoMath()
    {
        if (!Double.isNaN(ResultValue)) {
            String textFormula = Res.getText().toString();
            int index = textFormula.indexOf(Action);
            if (index != -1) {
                String numberValue = textFormula.substring(index + 1);
                Value = Double.parseDouble(numberValue);
                switch (Action) {
                    case '/':
                        if (Value == 0) {
                            ResultValue = 0.0;
                        } else {
                            ResultValue /= Value;
                        }
                        break;
                    case '*':
                        ResultValue *= Value;
                        break;
                    case '+':
                        ResultValue += Value;
                        break;
                    case '-':
                        ResultValue -= Value;
                        break;
                    case '%':
                        ResultValue = ResultValue/100 * Value;
                        break;
                    case '=':
                        ResultValue = Value;
                        break;
                }
            }
        }
        else
        {
            try {
                ResultValue = Double.parseDouble(Res.getText().toString());
            } catch (Exception e){
                e.printStackTrace();
                ResultValue = 0.0;
            }
        }
        Res.setText(String.valueOf(ResultValue));
    }

    private void MegaGoMath()
    {
        if (!Double.isNaN(ResultValue) && ResultValue != 0.0) {
            String textFormula = Res.getText().toString();
            int index = textFormula.indexOf(Action);
            if (index != -1) {
                switch (Action) {
                    case '√':
                        ResultValue = Math.sqrt(ResultValue);
                        break;
                    case '²':
                        ResultValue = Math.pow(ResultValue, 2);
                        break;
                }
            }
        }
        else
        {
            try {
                ResultValue = Double.parseDouble(Res.getText().toString());
            } catch (Exception e) {
                e.printStackTrace();
                ResultValue = 0.0;
            }
        }
    }
}