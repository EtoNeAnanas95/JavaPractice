package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Castle {
    public static long _amountCitizens = 0;
    private static long _amountArmy = 0;
    private static long _treasury = 0;
    private static List<String> relationsCastles = new ArrayList<>();
    private  List<String> warCastles = new ArrayList<>();
    private Scanner _sc = new Scanner(System.in);
    private String castleName;

    public Castle(long amountCitizens, String castleName) {
        if (amountCitizens < 0) {
            System.out.println("Граждан не может быть меньше нуля");
            return;
        }
        _amountCitizens = amountCitizens;
        this.castleName = new String();
        this.castleName = castleName;
        System.out.println("В вашем замке " + amountCitizens + " граждан");
    }

    public static void addArmyOrRemove(long amountSolders) {
        if (amountSolders < 0 && amountSolders > _amountArmy) {
            System.out.println("Нельзя убрать солдат больше, чем есть по факту");
            return;
        }
        _amountArmy += amountSolders;
        System.out.println("В вашем замке " + amountSolders + " солдат");
    }

    public static void addTreasuryOrRemove(long amountGold) {
        _treasury += amountGold;
        if (_treasury < 0) {
            System.out.println("Казна и так пуста");
            _treasury = 0;
            return;
        }
        System.out.println("В вашем замке " + _treasury + " золота");
    }

    public static void doExecute(int amountExecutedCitizens) {
        if (amountExecutedCitizens > _amountCitizens) {
            System.out.println("Нельзя казнить столько людей, в замке всего " + _amountCitizens + " граждан");
            return;
        }else if (amountExecutedCitizens < 0) {
            System.out.println("Количество людей на казьнь, должно быть больше нуля");
            return;
        }
        _amountCitizens -= amountExecutedCitizens;
    }

    public void capitulate() {
        _amountCitizens = 0;
        _amountArmy = 0;
        _treasury = 0;
        System.out.println("Вы капитулировали");
    }

    public void setTradeRelations() {
        System.out.println("Введите название королевства с которым хотите уставноить торговые отношения");
        relationsCastles.add(_sc.nextLine());
    }
    
    public void announceWar() {
        System.out.println("Введите название королевства которому хотите объявить войну");
        warCastles.add(_sc.nextLine());
    }
    
    //функции, которые являются логическим продолжением другиз стандартных функций
    public void displayTradeRelations() { //~~~функция для вывода текущих торговых отношений~~~
        System.out.println("Торговые отношения установлены с следующими королевствами:");
        for (String castle : relationsCastles) {
            System.out.println(castle);
        }
    }

    public void removeTradeRelations() { //~~~функция для разрыва торговых отношений~~~
        System.out.println("Выберите номер каролевства с которым хотите разорвать торги");
        byte i = 0;
        for (String castle : relationsCastles) {
            System.out.println(i + ". " + castle);
            i++;
        }
        byte numberCastle = _sc.nextByte();
        if (numberCastle >= 0 && numberCastle <= relationsCastles.size()) relationsCastles.remove(numberCastle);
    }

    public void displayWarCastles() { //~~~функция для вывода текущих войн~~~
        System.out.println("Война идёт с следующими королевствами:");
        for (String castle : warCastles) {
            System.out.println(castle);
        }
    }

    public void removeWarCastles() { //~~~функция для мира с королевствами~~~
        System.out.println("Выберите номер каролевства с которым хотите пойти на мировую");
        byte i = 0;
        for (String castle : warCastles) {
            System.out.println(i + ". " + castle);
            i++;
        }
        byte numberCastle = _sc.nextByte();
        if (numberCastle >= 0 && numberCastle <= warCastles.size()) warCastles.remove(numberCastle);
    }
}
