package com.kirilov.pineapplemod.items;

import com.kirilov.pineapplemod.PineappleMod;
import com.kirilov.pineapplemod.blocks.ModBlock;
import net.minecraft.world.food.FoodProperties;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModItem {
    public static final DeferredRegister<Item> ITEMS = 
            DeferredRegister.create(ForgeRegistries.ITEMS, PineappleMod.MOD_ID);


    public static final RegistryObject<Item> PINEAPPLE = 
            ITEMS.register("pineapple", () -> new Item(new Item.Properties()
            .food((new FoodProperties
                    .Builder()
                    .nutrition(10).saturationMod(0.9f))
                    .build()).tab(CreativeModeTab.TAB_FOOD)));

    public static final RegistryObject<Item> GOLDEN_PINEAPPLE = 
            ITEMS.register("golden_pineapple", () -> new Item(new Item.Properties()
            .food((new FoodProperties
                    .Builder()
                    .nutrition(20)
                    .saturationMod(0.5f))
                    .build()).tab(CreativeModeTab.TAB_FOOD)));

    public static final RegistryObject<Item> PIECE_OF_PINEAPPLE = 
            ITEMS.register("piece_of_pineapple", () -> new Item(new Item.Properties()
            .food((new FoodProperties
                    .Builder()
                    .nutrition(3)
                    .saturationMod(0.1f))
                    .build()).tab(CreativeModeTab.TAB_FOOD)));

    public static final RegistryObject<Item> PALM_WOOD = 
            ITEMS.register("palm_wood", () -> new BlockItem(ModBlock
                    .PALM_WOOD.get(), new Item.Properties().tab(CreativeModeTab.TAB_BUILDING_BLOCKS)));

    public static final RegistryObject<Item> PALM_LEAVES = 
            ITEMS.register("palm_leaves", () -> new BlockItem(ModBlock
                    .PALM_LEAVES.get(), new Item.Properties().tab(CreativeModeTab.TAB_BUILDING_BLOCKS)));

    public static final RegistryObject<Item> PINEAPPLE_BLOCK = 
            ITEMS.register("pineapple_block", () -> new BlockItem(ModBlock
                    .PINEAPPLE_BLOCK.get(), new Item.Properties().tab(CreativeModeTab.TAB_FOOD)));

    public static final RegistryObject<Item> GOLDEN_PINEAPPLE_BLOCK = 
            ITEMS.register("golden_pineapple_block", () -> new BlockItem(ModBlock
                    .GOLDEN_PINEAPPLE_BLOCK.get(), new Item.Properties().tab(CreativeModeTab.TAB_FOOD)));

    public static void register(IEventBus eventBus) {
        ITEMS.register(eventBus);
    }

}
