package com.kirilov.pineapplemod.blocks;

import com.kirilov.pineapplemod.PineappleMod;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;


public class ModBlock {
    public static final DeferredRegister<Block> BLOCKS = 
            DeferredRegister.create(ForgeRegistries.BLOCKS, PineappleMod.MOD_ID);

    public static final RegistryObject<Block> PALM_WOOD = 
            BLOCKS.register("palm_wood", () ->
                    new Block(BlockBehaviour.Properties
                            .of(Material.WOOD)
                            .strength(2.0f)
                    )
            );

    public static final RegistryObject<Block> PALM_LEAVES = 
            BLOCKS.register("palm_leaves", () ->
                    new Block(BlockBehaviour.Properties
                            .of(Material.LEAVES))
            );

    public static final RegistryObject<Block> PINEAPPLE_BLOCK =
            BLOCKS.register("pineapple_block", () ->
                    new Block(BlockBehaviour.Properties
                            .of(Material.HEAVY_METAL)
                            .strength(1.0f)
                            .requiresCorrectToolForDrops()
                    )
            );

    public static final RegistryObject<Block> GOLDEN_PINEAPPLE_BLOCK =
            BLOCKS.register("golden_pineapple_block", () ->
                    new Block(BlockBehaviour.Properties
                            .of(Material.HEAVY_METAL)
                            .strength(2.0f)
                            .lightLevel((state) -> 15)
                    )
            );

    public static void register(IEventBus eventBus) {
        BLOCKS.register(eventBus);
    }

}
