package com.kirilov.pineapplemod.items;

import com.kirilov.pineapplemod.PineappleMod;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.item.alchemy.Potion;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModPotion {
    public static final DeferredRegister<Potion> POTIONS
            = DeferredRegister.create(ForgeRegistries.POTIONS, PineappleMod.MOD_ID);
    
    public static final RegistryObject<Potion> KAIF = POTIONS.register("kaif",
            () -> new Potion(new MobEffectInstance(MobEffects.REGENERATION, 999999 * 20, 3),
                    new MobEffectInstance(MobEffects.MOVEMENT_SPEED, 999999 * 20, 5),
                    new MobEffectInstance(MobEffects.ABSORPTION, 999999 * 20),
                    new MobEffectInstance(MobEffects.SLOW_FALLING, 999999 * 20)));

    public static void register(IEventBus eventBus) {
        POTIONS.register(eventBus);
    }

}
