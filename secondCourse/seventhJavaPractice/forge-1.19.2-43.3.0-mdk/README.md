<h1>Mod description</h1>
<p>This is my first mod. It adds some three new types of food to the game. A few blocks and a potion</p>
<details>
<summary>Blocks</summary>
1. Palm wood block <br>
2. Palm leaves block <br>
3. Pineapple block <br>
4. Golden pineapple block <br>
</details>
<hr>
<details>
<summary>Food</summary>
1. Pineapple<br>
2. Golden pineapple<br>
</details>
<hr>
<details>
<summary>Potion</summary>
1. Potion of kaif<br>
</details>
