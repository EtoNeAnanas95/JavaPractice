package org.example;

import org.example.CastleClasses.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Castle {
    private static Scanner scanner = new Scanner(System.in);
    public static void run() {
        Court court = new Court(scanner);
        Treasury treasury = new Treasury(scanner);
        War war = new War();
        Tavern tavern = new Tavern(scanner);

        while (true) {
            System.out.println("Выберите действие:");
            System.out.println("1) суд");
            System.out.println("2) работа с казной");
            System.out.println("3) бой с врагом");
            System.out.println("4) новый гражданин");
            System.out.println("5) таверна");

            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    court.mainDuty();
                    break;
                case 2:
                    _runMain(treasury);
                    break;
                case 3:
                    _runMain(war);
                    break;
                case 5:
                    tavern.mainDuty();
                    break;
                case 4:
                    court.AddCitizen();
                    break;
                default:
                    System.out.println("Выберите число от 1 до 5");
                    break;
            }
        }
    }

    private static void _runMain(Enterprise go) {
        List<String> values = new ArrayList<>();
        System.out.println(go.args[0]);
        if (go.ifJustList) {
            for (String item : go.args) {
                System.out.println(item);
            }
            values.add(scanner.nextLine());
        } else {
            int i = 1;
            for (String item : go.args ) {
                if (i == 1) {
                    i++;
                    continue;
                }
                System.out.println(item);
                values.add(scanner.nextLine());
                scanner.nextLine();
            }
        }
        go.mainDuty(values.toArray(new String[values.size()]));
    }
}
