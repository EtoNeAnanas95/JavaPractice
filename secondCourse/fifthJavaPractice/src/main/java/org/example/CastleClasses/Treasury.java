package org.example.CastleClasses;

import java.util.Scanner;

public class Treasury extends Enterprise {
    private double _treasure = 0;
    private Scanner _sc;
    public Treasury(Scanner scanner) {
        this.args = new String[] {"Выберите действие:", "1) показать казну", "2) пополнить казну", "3) опустошить казну"};
        this._sc = scanner;
        this.ifJustList = true;
    }

    @Override
    public void mainDuty(String... parametrs) {
        int choice = Integer.parseInt(parametrs[0]);
        switch (choice) {
            case 1:
                System.out.println("Текущая казна: " + _treasure);
                break;
            case 2:
                _addToTrasury();
                break;
            case 3:
                _removeFromTrasury();
                break;
            default:
                System.out.println("Такого действия нет");
                break;
        }
    }

    private void _addToTrasury() {
        System.out.println("Введите сумму, которая будет добавлена к казне: ");
        double amount = _sc.nextDouble();
        _sc.nextLine();
        _treasure += amount;
        System.out.println("Текущая казна: " + _treasure);
    }

    private void _removeFromTrasury() {
        System.out.println("Введите сумму, которая будет вычтена из казны: ");
        double amount = _sc.nextDouble();
        _sc.nextLine();
        _treasure -= amount;
        System.out.println("Текущая казна: " + _treasure);
    }
}
