package org.example.CastleClasses;

import java.util.Scanner;

public class Tavern {
    private Scanner _sc;
    public Tavern(Scanner scanner) {
        this._sc = scanner;
    }

    public void mainDuty() {
        System.out.println("Выберите алкоголь, который вы будете пить:");
        String drink = _sc.nextLine();
        System.out.println("Держи своё " + drink);
    }
}
