package org.example;

import java.io.IOException;

public class Main {
    static Log _log;
    static {
        try {
            _log = new Log("main");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static void main(String[] args) {
        try {Castle.run();} catch (Exception e) {_log.logger.warning("Uncaught exception " + e);}
    }
}