package org.example.CastleClasses;

import org.example.Log;

import java.io.IOException;

public class War extends Enterprise {
    static Log _log;
    static {
        try {
            _log = new Log("War");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public War() {
        this.args = new String[]{"Введите число боеприпасов, которое будет выпущено в противника:", "Введите число арбалетных болтов:", "Введите число пушечных шаров:"};
    }

    @Override
    public void mainDuty(String... parametrs) {
        int bolts = Integer.parseInt(parametrs[0]);
        int balls = Integer.parseInt(parametrs[1]);
        int totalAmmo = bolts + balls;
        System.out.println("Общее число боеприпасов: " + totalAmmo);
        _log.logger.fine("Main duty completed successfully.");
    }
}
