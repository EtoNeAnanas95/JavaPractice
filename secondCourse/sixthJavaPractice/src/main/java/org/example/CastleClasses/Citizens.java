package org.example.CastleClasses;

import org.example.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Citizens {
    static Log _log;
    static {
        try {
            _log = new Log("Citizens");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    protected ArrayList<String> _citizens = new ArrayList<>();
    protected Scanner _sc;

    public void AddCitizen() {
        System.out.println("Введите имя новго гражданина");
        String name = _sc.nextLine();
        _citizens.add(name);
        _log.logger.info("Citizen was added: " + name);
    }
}
