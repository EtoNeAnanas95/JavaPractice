package org.example.CastleClasses;

import org.example.Log;

import java.io.IOException;
import java.util.Scanner;

public class Court extends Citizens {
    static Log _log;
    static {
        try {
            _log = new Log("Court");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public Court(Scanner scanner) {
        this._sc = scanner;
    }

    public void mainDuty() {
        System.out.println("Введите имя человека, над которым будет проходить суд: ");
        String name = _sc.nextLine();

        System.out.println("Выберите действие: ");
        System.out.println("1) казнить");
        System.out.println("2) помиловать");

        int choice = _sc.nextInt();
        _sc.nextLine();

        switch (choice) {
            case 1:
                System.out.println("Будет казнён человек с именем: " + name);
                _citizens.remove(name);
                break;
            case 2:
                System.out.println("Будет помилован человек с именем: " + name);
                break;
            default:
                System.out.println("Такого варианта нет!");
                break;
        }
        _log.logger.info("Main duty completed successfully");
    }
}
