package org.example.CastleClasses;

import org.example.Log;

import java.io.IOException;
import java.util.Scanner;

public class Tavern {
    static Log _log;
    static {
        try {
            _log = new Log("Tavern");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    private Scanner _sc;
    public Tavern(Scanner scanner) {
        this._sc = scanner;
    }

    public void mainDuty() {
        System.out.println("Выберите алкоголь, который вы будете пить:");
        String drink = _sc.nextLine();
        System.out.println("Держи своё " + drink);
        _log.logger.info("Main duty completed successfully");
    }
}
