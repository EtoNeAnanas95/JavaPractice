package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<Fruit> fruits = new ArrayList<Fruit>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        createFruits();

        RecyclerView recyclerView = findViewById(R.id.main_panel);

        RecyclerAdapter.OnClickListener onClickListener = new RecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(Fruit fruit, int position) {
                Toast.makeText(getApplicationContext(), "Selected: " + fruit.getName(),
                        Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, FullCardInfo.class);
                intent.putExtra(Fruit.class.getSimpleName(), fruit);
                startActivity(intent);
            }
        };

        RecyclerAdapter adapter = new RecyclerAdapter(MainActivity.this, fruits, onClickListener);
        recyclerView.setAdapter(adapter);
    }

    private void createFruits() {
        fruits.add(new Fruit("PINE APPLE", "AHAHHA", R.drawable.pineapple));
        fruits.add(new Fruit("MEGA PINE APPLE", "AHAHHA", R.drawable.alchogol_pinepaple));
        fruits.add(new Fruit("MEGA PINE APPLE", "AHAHHA", R.drawable.negative_pineapple));
        fruits.add(new Fruit("MEGA PINE APPLE", "AHAHHA", R.drawable.reverse_pineapple));

        fruits.add(new Fruit("PINE APPLE", "AHAHHA", R.drawable.pineapple));
        fruits.add(new Fruit("MEGA PINE APPLE", "AHAHHA", R.drawable.alchogol_pinepaple));
        fruits.add(new Fruit("MEGA PINE APPLE", "AHAHHA", R.drawable.negative_pineapple));
        fruits.add(new Fruit("MEGA PINE APPLE", "AHAHHA", R.drawable.reverse_pineapple));

        fruits.add(new Fruit("PINE APPLE", "AHAHHA", R.drawable.pineapple));
        fruits.add(new Fruit("MEGA PINE APPLE", "AHAHHA", R.drawable.alchogol_pinepaple));
        fruits.add(new Fruit("MEGA PINE APPLE", "AHAHHA", R.drawable.negative_pineapple));
        fruits.add(new Fruit("MEGA PINE APPLE", "AHAHHA", R.drawable.reverse_pineapple));

    }
}