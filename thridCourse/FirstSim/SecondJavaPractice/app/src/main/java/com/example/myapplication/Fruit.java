package com.example.myapplication;

import java.io.Serializable;

public class Fruit implements Serializable {
    private String name;
    private String description;
    private int image;

    public Fruit(String name, String description, int photoResource) {
        this.name = name;
        this.description = description;
        image = photoResource;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int get_photoResource() {
        return image;
    }

    public void set_photoResource(int _photoResource) {
        this.image = _photoResource;
    }
}
