package com.example.myapplication;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

public class FullCardInfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_full_card_info);


        ImageView img = findViewById(R.id.full_image);
        TextView description = findViewById(R.id.full_description);
        TextView name = findViewById(R.id.full_name);

        Bundle extras = getIntent().getExtras();
        Fruit fruit;
        if (extras != null) {
            fruit = (Fruit) extras.getSerializable(Fruit.class.getSimpleName());
            img.setImageResource(fruit.get_photoResource());
            name.setText(fruit.getName());
            description.setText(fruit.getDescription());
        }

        Button backButton = findViewById(R.id.go_back);

        backButton.setOnClickListener(v -> finish());
    }
}