package com.example.myapplication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private final OnClickListener onClickListener;
    private final LayoutInflater inflater;
    private final ArrayList<Fruit> fruits;
    public RecyclerAdapter(Context context, ArrayList<Fruit> fruits, OnClickListener onClickListener) {
        this.fruits = fruits;
        this.inflater = LayoutInflater.from(context);
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Fruit fruit = fruits.get(position);
        holder.imageView.setImageResource(fruit.get_photoResource());
        holder.nameView.setText(fruit.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(fruit, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return fruits.size();
    }

    interface OnClickListener {
        void onClick(Fruit fruit, int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView imageView;
        final TextView nameView;

        ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.image);
            nameView = view.findViewById(R.id.name);
        }
    }
}
