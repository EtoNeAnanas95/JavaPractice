package com.example.third_java_practice

import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import kotlin.random.Random

class BotActivity : AppCompatActivity() {
    private val buttons = arrayOfNulls<Button>(9)
    private var movePlayer = true
    private var isPlayer = true
    private var charPlayer = "X"
    private var charBot = "O"
    private lateinit var settings: SharedPreferences
    private lateinit var settingsEditor: SharedPreferences.Editor

    override fun onCreate(savedInstanceState: Bundle?) {
        settings = getSharedPreferences("SETTINGS", MODE_PRIVATE)
        settingsEditor = settings.edit()

        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_bot)
        initBoardGame()
        updateScores()

        findViewById<Button>(R.id.reset).setOnClickListener {
            resetGameBoard();
            settingsEditor.putInt("PLAYER", 0)
            settingsEditor.putInt("BOT", 0)
            settingsEditor.putInt("DRAWS_IN_BOT", 0)
            settingsEditor.apply()
            movePlayer = true
            isPlayer = true
            charPlayer = "X"
            charBot = "O"
            updateScores()
        }
    }

    private fun initBoardGame() {
        for (index in buttons.indices) {
            val buttonId: String = "button${index}"
            val resourceId: Int = resources.getIdentifier(buttonId, "id", packageName)
            buttons[index] = findViewById(resourceId)
            buttons[index]?.setOnClickListener { onButtonClick(it as Button) }

        }
    }

    private fun bot() {
        var isMove = false;
        do {
            val buttonIndex = Random.nextInt(0, 9)
            isMove = move(buttons[buttonIndex] as Button)
        } while (!isMove)
    }

    private fun onButtonClick(button: Button) {
        val isMove = move(button)
        if (!isMove) return
        checkForWinner()
        bot()
        checkForWinner()
    }

    private fun move(button: Button): Boolean {
        if (!button.text.isNullOrEmpty()) return false

        button.text = if (movePlayer) charPlayer else charBot
        movePlayer = !movePlayer
        return true
    }

    private fun checkForWinner() {
        var isDraw = true
        buttons.forEach { button -> if (button?.text.isNullOrEmpty()) isDraw = false }

        if ((buttons[0]?.text == charPlayer &&
                    buttons[1]?.text == charPlayer &&
                    buttons[2]?.text == charPlayer) ||
            (buttons[3]?.text == charPlayer &&
                    buttons[4]?.text == charPlayer &&
                    buttons[5]?.text == charPlayer) ||
            (buttons[6]?.text == charPlayer &&
                    buttons[7]?.text == charPlayer &&
                    buttons[8]?.text == charPlayer) ||
            (buttons[0]?.text == charPlayer &&
                    buttons[3]?.text == charPlayer &&
                    buttons[6]?.text == charPlayer) ||
            (buttons[1]?.text == charPlayer &&
                    buttons[4]?.text == charPlayer &&
                    buttons[7]?.text == charPlayer) ||
            (buttons[2]?.text == charPlayer &&
                    buttons[5]?.text == charPlayer &&
                    buttons[8]?.text == charPlayer) ||
            (buttons[0]?.text == charPlayer &&
                    buttons[4]?.text == charPlayer &&
                    buttons[8]?.text == charPlayer) ||
            (buttons[2]?.text == charPlayer &&
                    buttons[4]?.text == charPlayer &&
                    buttons[6]?.text == charPlayer)
        ) {
            val win = settings.getInt("PLAYER", 0)
            settingsEditor.putInt("PLAYER", win + 1)
            settingsEditor.apply()
            updateScores();
            resetGameBoard();
        } else if ((buttons[0]?.text == charBot &&
                    buttons[1]?.text == charBot &&
                    buttons[2]?.text == charBot) ||
            (buttons[3]?.text == charBot &&
                    buttons[4]?.text == charBot &&
                    buttons[5]?.text == charBot) ||
            (buttons[6]?.text == charBot &&
                    buttons[7]?.text == charBot &&
                    buttons[8]?.text == charBot) ||
            (buttons[0]?.text == charBot &&
                    buttons[3]?.text == charBot &&
                    buttons[6]?.text == charBot) ||
            (buttons[1]?.text == charBot &&
                    buttons[4]?.text == charBot &&
                    buttons[7]?.text == charBot) ||
            (buttons[2]?.text == charBot &&
                    buttons[5]?.text == charBot &&
                    buttons[8]?.text == charBot) ||
            (buttons[0]?.text == charBot &&
                    buttons[4]?.text == charBot &&
                    buttons[8]?.text == charBot) ||
            (buttons[2]?.text == charBot &&
                    buttons[4]?.text == charBot &&
                    buttons[6]?.text == charBot)
        ) {
            val win = settings.getInt("BOT", 0)
            settingsEditor.putInt("BOT", win + 1)
            settingsEditor.apply()
            updateScores();
            resetGameBoard();
        } else if (isDraw) {
            val draw = settings.getInt("DRAWS_IN_BOT", 0);
            settingsEditor.putInt("DRAWS_IN_BOT", draw + 1);
            settingsEditor.apply();
            updateScores();
            resetGameBoard();
        }
    }

    private fun updateScores() {
        findViewById<TextView>(R.id.wins).text = "Play: ${settings.getInt("PLAYER", 0)}"
        findViewById<TextView>(R.id.losses).text = "Bot: ${settings.getInt("BOT", 0)}"
        findViewById<TextView>(R.id.draws).text = "Draw: ${settings.getInt("DRAWS_IN_BOT", 0)}"
    }

    private fun resetGameBoard() {
        buttons.forEach { button ->
            button?.text = ""
        }
        isPlayer = !isPlayer
        movePlayer = isPlayer
        val buffer = charPlayer
        charPlayer = charBot
        charBot = buffer
    }
}
