package com.example.third_java_practice;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

public class PlayerActivity extends AppCompatActivity {

    private Button[] buttons = new Button[9];
    private boolean moveFirstPlayer = true;
    private boolean isFirstPlayer = true;
    private String charFirstPlayer = "X";
    private String charSecondPlayer = "O";
    private SharedPreferences settings;
    private SharedPreferences.Editor settingsEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        settings = getSharedPreferences("SETTINGS", MODE_PRIVATE);
        settingsEditor = settings.edit();

        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_player);
        updateScores();
        initializeGameBoard();
        ((Button) findViewById(R.id.reset)).setOnClickListener((v) -> {
            resetGameBoard();
            settingsEditor.putInt("PLAYER1", 0);
            settingsEditor.putInt("PLAYER2", 0);
            settingsEditor.putInt("DRAW", 0);
            settingsEditor.apply();
            moveFirstPlayer = true;
            isFirstPlayer = true;
            charFirstPlayer = "X";
            charSecondPlayer = "O";
            updateScores();
        });
    }

    private void initializeGameBoard() {
        for (int buttonIndex = 0; buttonIndex < 9; buttonIndex++) {
            String buttonID = "button" + buttonIndex;
            int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
            buttons[buttonIndex] = findViewById(resID);
            buttons[buttonIndex].setOnClickListener(v -> onButtonClick((Button) v));
        }
    }

    private void onButtonClick(Button button) {
        if (!button.getText().toString().equals("")) return;

        button.setText(moveFirstPlayer ? charFirstPlayer : charSecondPlayer);
        moveFirstPlayer = !moveFirstPlayer;

        checkForWinner();
    }

    private void checkForWinner() {
        boolean isDraw = true;
        for (Button button : buttons) {
            if (button.getText().equals(""))
                isDraw = false;
        }
        if ((buttons[0].getText() == charFirstPlayer &&
                buttons[1].getText() == charFirstPlayer &&
                buttons[2].getText() == charFirstPlayer) ||
                (buttons[3].getText() == charFirstPlayer &&
                        buttons[4].getText() == charFirstPlayer &&
                        buttons[5].getText() == charFirstPlayer) ||
                (buttons[6].getText() == charFirstPlayer &&
                        buttons[7].getText() == charFirstPlayer &&
                        buttons[8].getText() == charFirstPlayer) ||
                (buttons[0].getText() == charFirstPlayer &&
                        buttons[3].getText() == charFirstPlayer &&
                        buttons[6].getText() == charFirstPlayer) ||
                (buttons[1].getText() == charFirstPlayer &&
                        buttons[4].getText() == charFirstPlayer &&
                        buttons[7].getText() == charFirstPlayer) ||
                (buttons[2].getText() == charFirstPlayer &&
                        buttons[5].getText() == charFirstPlayer &&
                        buttons[8].getText() == charFirstPlayer) ||
                (buttons[0].getText() == charFirstPlayer &&
                        buttons[4].getText() == charFirstPlayer &&
                        buttons[8].getText() == charFirstPlayer) ||
                (buttons[2].getText() == charFirstPlayer &&
                        buttons[4].getText() == charFirstPlayer &&
                        buttons[6].getText() == charFirstPlayer)) {
            int win = settings.getInt("PLAYER1", 0);
            settingsEditor.putInt("PLAYER1", win + 1);
            settingsEditor.apply();
            updateScores();
            resetGameBoard();
        } else if ((buttons[0].getText() == charSecondPlayer &&
                buttons[1].getText() == charSecondPlayer &&
                buttons[2].getText() == charSecondPlayer) ||
                (buttons[3].getText() == charSecondPlayer &&
                        buttons[4].getText() == charSecondPlayer &&
                        buttons[5].getText() == charSecondPlayer) ||
                (buttons[6].getText() == charSecondPlayer &&
                        buttons[7].getText() == charSecondPlayer &&
                        buttons[8].getText() == charSecondPlayer) ||
                (buttons[0].getText() == charSecondPlayer &&
                        buttons[3].getText() == charSecondPlayer &&
                        buttons[6].getText() == charSecondPlayer) ||
                (buttons[1].getText() == charSecondPlayer &&
                        buttons[4].getText() == charSecondPlayer &&
                        buttons[7].getText() == charSecondPlayer) ||
                (buttons[2].getText() == charSecondPlayer &&
                        buttons[5].getText() == charSecondPlayer &&
                        buttons[8].getText() == charSecondPlayer) ||
                (buttons[0].getText() == charSecondPlayer &&
                        buttons[4].getText() == charSecondPlayer &&
                        buttons[8].getText() == charSecondPlayer) ||
                (buttons[2].getText() == charSecondPlayer &&
                        buttons[4].getText() == charSecondPlayer &&
                        buttons[6].getText() == charSecondPlayer)) {
            int win = settings.getInt("PLAYER2", 0);
            settingsEditor.putInt("PLAYER2", win + 1);
            settingsEditor.apply();
            updateScores();
            resetGameBoard();
        } else if (isDraw) {
            int draw = settings.getInt("DRAW", 0);
            settingsEditor.putInt("DRAW", draw + 1);
            settingsEditor.apply();
            updateScores();
            resetGameBoard();
        }
    }

    private void updateScores() {
        ((TextView) findViewById(R.id.player1)).setText("Player1: " + settings.getInt("PLAYER1", 0));
        ((TextView) findViewById(R.id.player2)).setText("Player2: " + settings.getInt("PLAYER2", 0));
        ((TextView) findViewById(R.id.draws)).setText("Draw: " + settings.getInt("DRAW", 0));
    }

    private void resetGameBoard() {
        for (Button button : buttons) {
            button.setText("");
        }
        isFirstPlayer = !isFirstPlayer;
        moveFirstPlayer = isFirstPlayer;
        String buffer = charFirstPlayer;
        charFirstPlayer = charSecondPlayer;
        charSecondPlayer = buffer;
    }
}