package com.example.eto_ne_ananasbI95;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class DetailActivity extends AppCompatActivity {
    private EditText editTextName;
    private EditText editTextAuthor;
    private DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        dbHelper = new DatabaseHelper(this);

        int idBook = getIntent().getIntExtra("id", -1);
        Book book = dbHelper.getBookById((long) idBook);

        editTextName = findViewById(R.id.editTextNameUpdate);
        editTextAuthor = findViewById(R.id.editTextAuthorUpdate);

        editTextName.setText(book.getBookName());
        editTextAuthor.setText(book.getBookAuthor());

        Button updateBtn = findViewById(R.id.update);
        updateBtn.setOnClickListener(v -> updateBookToDatabase(idBook));

        Button deleteBtn = findViewById(R.id.delete);
        deleteBtn.setOnClickListener(v -> deleteBookToDatabase(idBook));
    }

    private void updateBookToDatabase(int id) {
        String bookName = editTextName.getText().toString().trim();
        String bookAuthor = editTextAuthor.getText().toString().trim();

        if (bookName.isEmpty() || bookAuthor.isEmpty()) {
            Toast.makeText(this, "Fill in all fields", Toast.LENGTH_SHORT).show();
            return;
        }

        int result = dbHelper.updateBookById((long) id, bookName, bookAuthor);

        if (result > 0) {
            Toast.makeText(this, "Book updated", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            Toast.makeText(this, "Book update error", Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteBookToDatabase(int id) {
        int result = dbHelper.deleteBookById((long) id);

        if (result > 0) {
            Toast.makeText(this, "Book deleted", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            Toast.makeText(this, "Book delete error", Toast.LENGTH_SHORT).show();
        }
    }
}