package com.example.eto_ne_ananasbI95;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private final List<Book> itemList;
    private final OnItemClickListener onItemClick;

    public interface OnItemClickListener {
        void onItemClick(Book book);
    }

    public RecyclerViewAdapter(List<Book> itemList, OnItemClickListener onItemClick) {
        this.itemList = itemList;
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Book book = itemList.get(position);
        holder.bookAuthor.setText(book.getBookAuthor());
        holder.bookName.setText(book.getBookName());
        holder.itemView.setOnClickListener(v -> onItemClick.onItemClick(book));
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView bookAuthor;
        public final TextView bookName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            bookAuthor = itemView.findViewById(R.id.b_author);
            bookName = itemView.findViewById(R.id.b_name);
        }
    }
}
