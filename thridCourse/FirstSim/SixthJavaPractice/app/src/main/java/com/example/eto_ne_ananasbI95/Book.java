package com.example.eto_ne_ananasbI95;

public class Book {
    private int idBook;
    private String bookName;
    private String bookAuthor;

    public Book(int idBook, String bookName, String bookAuthor) {
        this.idBook = idBook;
        this.bookName = bookName;
        this.bookAuthor = bookAuthor;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public int getIdBook() {
        return idBook;
    }

    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
}
