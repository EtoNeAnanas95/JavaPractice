package com.example.eto_ne_ananasbI95;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String COLUMN_ID = "id_book";
    public static final String COLUMN_NAME = "book_name";
    public static final String COLUMN_AUTHOR = "book_author";
    private static final String TABLE_NAME = "books";
    private static final String NAME_DB = "book_db";
    private static final int VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, NAME_DB, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableQuery = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME + " TEXT, " +
                COLUMN_AUTHOR + " TEXT)";
        db.execSQL(createTableQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public long addBook(String bookName, String bookAuthor) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValue = new ContentValues();
        contentValue.put(COLUMN_NAME, bookName);
        contentValue.put(COLUMN_AUTHOR, bookAuthor);
        return db.insert(TABLE_NAME, null, contentValue);
    }

    public Cursor getAllBooks() {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.query(TABLE_NAME, null, null, null, null, null, null);
    }

    public Book getBookById(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Book book = null;

        Cursor cursor = db.query(TABLE_NAME, null, COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)}, null, null, null);

        if (cursor.moveToFirst()) {
            int bookId = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_ID));
            String bookName = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME));
            String bookAuthor = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_AUTHOR));
            book = new Book(bookId, bookName, bookAuthor);
            cursor.close();
        }

        return book;
    }

    public int deleteBookById(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, COLUMN_ID + " = ?", new String[]{String.valueOf(id)});
    }

    public int updateBookById(long id, String bookName, String bookAuthor) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValue = new ContentValues();
        contentValue.put(COLUMN_NAME, bookName);
        contentValue.put(COLUMN_AUTHOR, bookAuthor);

        return db.update(TABLE_NAME, contentValue, COLUMN_ID + " = ?",
                new String[]{String.valueOf(id)});
    }
}
