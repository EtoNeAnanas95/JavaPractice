package com.example.firstjavapractice;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private Button rabbitBtn;
    private Button helicopterBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);

        rabbitBtn = findViewById(R.id.rabbit);
        helicopterBtn = findViewById(R.id.helicopter);



        rabbitBtn.setOnClickListener(f -> {
            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.move_from_right, R.anim.move_to_left);
        });

        helicopterBtn.setOnClickListener(f -> {
            Intent intent = new Intent(MainActivity.this, ThirdActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.maximaze, R.anim.fade);
        });

        Animation moveFromLeft = AnimationUtils.loadAnimation(MainActivity.this, R.anim.move_from_left);

        rabbitBtn.startAnimation(moveFromLeft);
        helicopterBtn.startAnimation(moveFromLeft);
    }
}
