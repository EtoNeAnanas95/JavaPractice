package com.example.firstjavapractice;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

public class ThirdActivity extends AppCompatActivity {

    private Button startBtn;
    private Button backBtn;
    private ImageView animationIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_thrid);

        animationIV = findViewById(R.id.anim_image_view);
        startBtn = findViewById(R.id.start);
        backBtn = findViewById(R.id.back);

        Animation moveLeft = AnimationUtils.loadAnimation(this, R.anim.move_to_left);
        Animation shuffle = AnimationUtils.loadAnimation(this, R.anim.shuffle);

        moveLeft.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animationIV.startAnimation(shuffle);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        startBtn.setOnClickListener(f -> {
            if (animationIV.getAnimation() == null) {
                animationIV.startAnimation(moveLeft);
            } else
                animationIV.clearAnimation();
        });

        backBtn.setOnClickListener(f -> {
            finish();
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation moveFromBottom = AnimationUtils.loadAnimation(ThirdActivity.this, R.anim.move_from_bottom);
                backBtn.setVisibility(View.VISIBLE);
                backBtn.startAnimation(moveFromBottom);
                startBtn.setVisibility(View.VISIBLE);
                startBtn.startAnimation(moveFromBottom);
                animationIV.setVisibility(View.VISIBLE);
                animationIV.startAnimation(moveFromBottom);
            }
        }, 600);
    }
}