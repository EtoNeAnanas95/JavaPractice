package com.example.firstjavapractice;

import android.annotation.SuppressLint;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    private Button startBtn;
    private Button backBtn;
    private ImageView animationIV;
    private AnimationDrawable frameAnimation;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_second);

        animationIV = findViewById(R.id.anim_image_view);
        startBtn = findViewById(R.id.start);
        backBtn = findViewById(R.id.back);
        frameAnimation = (AnimationDrawable) animationIV.getDrawable();


        startBtn.setOnClickListener(f -> {
            if (!frameAnimation.isRunning()) {
                startBtn.setText("Stop");
                frameAnimation.start();
            } else {
                startBtn.setText("Start");
                frameAnimation.stop();
            }
        });

        backBtn.setOnClickListener(f -> {
            finish();
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation moveFromRight = AnimationUtils.loadAnimation(SecondActivity.this, R.anim.move_from_right);
                backBtn.setVisibility(View.VISIBLE);
                backBtn.startAnimation(moveFromRight);
                startBtn.setVisibility(View.VISIBLE);
                startBtn.startAnimation(moveFromRight);
                animationIV.setVisibility(View.VISIBLE);
                animationIV.startAnimation(moveFromRight);
            }
        }, 600);
    }
}