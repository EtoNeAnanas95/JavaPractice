package com.example.seventhpractical;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.camera2.CameraManager;
import android.util.Log;

public class ToggleFlashlightReceiver extends BroadcastReceiver {
    public static boolean isFlashOn = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("Flash", "Вызван метод включения фонарика");
        CameraManager cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
        String cameraId;
        try {
            cameraId = cameraManager.getCameraIdList()[0];
            boolean isFlashOn = ToggleFlashlightReceiver.isFlashOn;
            cameraManager.setTorchMode(cameraId, !isFlashOn);
            ToggleFlashlightReceiver.isFlashOn = !isFlashOn;
        } catch (Exception e) {
            Log.e("Flash", "Ошибка при управлении фонариком: " + e.getMessage());
        }
    }
}
