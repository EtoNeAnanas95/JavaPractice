package com.example.autoris;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class AuthActivity extends AppCompatActivity {
    private EditText emailField;
    private EditText passwordField;
    private Button loginBtn;
    private Button registerBtn;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        auth = FirebaseAuth.getInstance();
        emailField = findViewById(R.id.emailField);
        passwordField = findViewById(R.id.passwordField);
        loginBtn = findViewById(R.id.loginButton);
        registerBtn = findViewById(R.id.registerButton);

        loginBtn.setOnClickListener(v -> loginUser());
        registerBtn.setOnClickListener(v -> registerUser());
    }

    private void registerUser() {
        String email = emailField.getText().toString();
        String password = passwordField.getText().toString();

        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show();
            return;
        }
        if (password.length() < 6) {
            Toast.makeText(this, "Пароль менее 6 символов", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(this, "Неверный формат почты", Toast.LENGTH_SHORT).show();
            return;
        }

        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, task -> {
            if (task.isSuccessful()) {
                Toast.makeText(this, "Регистрация прошла успешно", Toast.LENGTH_SHORT).show();
                saveUserToFirestore(email, password);
            } else {
                if (task.getException() != null) {
                    String error = task.getException().getMessage();
                    Toast.makeText(this, "Ошибка " + error, Toast.LENGTH_SHORT).show();
                    Log.e("AuthError", "Ошибка регистрации", task.getException());
                }
            }
        });
    }

    private void loginUser() {
        String email = emailField.getText().toString();
        String password = passwordField.getText().toString();

        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show();
            return;
        }

        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, task -> {
            if (task.isSuccessful()) {
                checkUserRole();
            } else {
                Toast.makeText(this, "Ошибка авторизации", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void saveUserToFirestore(String email, String password) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        User user = new User(email, password, "user");
        user.setId(auth.getCurrentUser().getUid());

        db.collection("users").document(user.getId()).set(user)
                .addOnSuccessListener(aVoid -> {
                    Toast.makeText(this, "Данные пользователя успешно сохранены", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(AuthActivity.this, MainActivity.class));
                    finish();
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(this, "Данные пользователя не сохранены: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                });
    }

    private void checkUserRole() {
        if (auth.getCurrentUser() != null) {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection("users").document(auth.getCurrentUser().getUid()).get()
                    .addOnSuccessListener(documentSnapshot -> {
                        String role = documentSnapshot.getString("role");
                        if (role != null) {
                            Intent intent;
                            switch (role) {
                                case "admin":
                                    intent = new Intent(AuthActivity.this, AdminActivity.class);
                                    break;
                                case "employee":
                                    intent = new Intent(AuthActivity.this, EmployeeActivity.class);
                                    break;
                                default:
                                    intent = new Intent(AuthActivity.this, MainActivity.class);
                                    break;
                            }
                            startActivity(intent);
                            finish();
                        }
                    });
        }
    }
}

