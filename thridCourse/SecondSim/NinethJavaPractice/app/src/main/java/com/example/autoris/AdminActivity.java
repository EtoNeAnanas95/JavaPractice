package com.example.autoris;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class AdminActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        Button serviceBtn = findViewById(R.id.serviceBtn);
        Button userBtn = findViewById(R.id.userBtn);
        Button exitBtn = findViewById(R.id.exitBtn);

        serviceBtn.setOnClickListener(v -> startActivity(new Intent(AdminActivity.this, ServiceActivity.class)));

        userBtn.setOnClickListener(v -> startActivity(new Intent(AdminActivity.this, UserActivity.class)));

        exitBtn.setOnClickListener(v -> {
            startActivity(new Intent(AdminActivity.this, AuthActivity.class));
            finish();
        });
    }
}