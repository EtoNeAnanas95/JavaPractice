package com.example.autoris;

import android.content.ClipData;
import android.view.View;

public interface ViewBinder<T> {
    void bindView(T item, View itemView);
}
