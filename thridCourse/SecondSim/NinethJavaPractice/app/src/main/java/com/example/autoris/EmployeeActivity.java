package com.example.autoris;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class EmployeeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee);
        Button serviceBtn = findViewById(R.id.serviceEmployeeBtn);
        Button exitBtn = findViewById(R.id.exitEmployeeBtn);

        serviceBtn.setOnClickListener(v -> startActivity(new Intent(EmployeeActivity.this, ServiceActivity.class)));

        exitBtn.setOnClickListener(v -> {
            startActivity(new Intent(EmployeeActivity.this, AuthActivity.class));
            finish();
        });
    }
}