package com.example.autoris;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.type.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private TextView dateTimeText;
    private Service selectedService;
    private List<Service> services;
    private List<Service> servicesFiltered;
    private RecyclerViewAdapter<Service> adapter;
    private FirebaseFirestore db;
    private FirebaseAuth auth;
    private String selectedDate = "";
    private String selectedTime = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dateTimeText = findViewById(R.id.dateTimeText);
        services = new ArrayList<>();
        servicesFiltered = new ArrayList<>();
        db = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();

        EditText searchEditText = findViewById(R.id.searchEditText);
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if (charSequence != null) {
                    String query = charSequence.toString();
                    servicesFiltered.clear();
                    if (query.isEmpty()) {
                        servicesFiltered.addAll(services);
                    } else {
                        for (Service service : services) {
                            if (service.getName().toLowerCase().contains(query.toLowerCase())) {
                                servicesFiltered.add(service);
                            }
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });

        ViewBinder<Service> binder = new ViewBinder<Service>() {
            @Override
            public void bindView(Service item, View itemView) {
                TextView textView = itemView.findViewById(R.id.nameService);
                textView.setText(item.getName());
                itemView.setOnClickListener(v -> {
                    selectedService = item;
                    showDateTimePicker();
                });
            }
        };

        RecyclerView recyclerView = findViewById(R.id.serviceRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RecyclerViewAdapter<>(servicesFiltered, binder, R.layout.service_card);
        recyclerView.setAdapter(adapter);

        findViewById(R.id.confirmButton).setOnClickListener(v -> {
            if (selectedService == null || selectedDate.isEmpty() || selectedTime.isEmpty()) {
                Toast.makeText(MainActivity.this, "Выберите сервис, дату и время", Toast.LENGTH_SHORT).show();
                return;
            }

            try {
                var date = new SimpleDateFormat("yyyy-MM-dd").parse(selectedDate);
                var today = new Date();
                if (date.before(today)) {
                    Toast.makeText(MainActivity.this, "Нельзя записаться на прошедшую дату", Toast.LENGTH_SHORT).show();
                    return;
                }
            } catch (ParseException e) {
                Log.e("ТЫ НАДОЕЛ", "be be be data plohaya");
                return;
            }

            if (auth.getCurrentUser() == null) {
                Toast.makeText(MainActivity.this, "Пользователь не автоизован", Toast.LENGTH_SHORT).show();
                return;
            }

            Appointment appointment = new Appointment(
                    auth.getCurrentUser().getUid(),
                    auth.getCurrentUser().getEmail() != null ? auth.getCurrentUser().getEmail() : "",
                    selectedService.getId().toString(),
                    selectedService.getName(),
                    selectedDate,
                    selectedTime
            );

            db.collection("appointments").add(appointment)
                    .addOnSuccessListener(documentReference -> {
                        Toast.makeText(MainActivity.this, "Запись успешно создана", Toast.LENGTH_SHORT).show();
                        selectedService = null;
                        selectedDate = "";
                        selectedTime = "";
                        dateTimeText.setText("Выбранные дата и время");
                    })
                    .addOnFailureListener(MainActivity.this::onFailure);
        });
        
        findViewById(R.id.exitBtn).setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, AuthActivity.class));
            finish();
        });

        loadData();
    }

    private void showDateTimePicker() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, (view, year, month, dayOfMonth) -> {
            TimePickerDialog timePickerDialog = new TimePickerDialog(this, (timeView, hourOfDay, minute) -> {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
                calendar.set(year, month, dayOfMonth, hourOfDay, minute);
                selectedDate = formatter.format(calendar.getTime());
                selectedTime = hourOfDay + ":" + minute;
                dateTimeText.setText("Выбранные дата и время: " + selectedDate + " " + selectedTime);
            }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
            timePickerDialog.show();
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private void loadData() {
        db.collection("service").get()
                .addOnSuccessListener(querySnapshot -> {
                    servicesFiltered.clear();
                    servicesFiltered.addAll(querySnapshot.toObjects(Service.class));
                    services.addAll(servicesFiltered);
                    adapter.notifyDataSetChanged();
                })
                .addOnFailureListener(this::onFailure);
    }

    private void onFailure(Exception ex) {
        Toast.makeText(this, "Ошибка: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
