package com.example.autoris;

import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class UserActivity extends AppCompatActivity {
    private Spinner roleSpinner;
    private EditText emailEdit, passwordEdit;
    private String selectedRole;
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private final List<User> users = new ArrayList<>();
    private RecyclerViewAdapter<User> adapter;
    private String selectedId = "";
    private final FirebaseAuth auth = FirebaseAuth.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        emailEdit = findViewById(R.id.nameEdit);
        passwordEdit = findViewById(R.id.passwordEdit);
        roleSpinner = findViewById(R.id.roleSpinner);

        spinnerInit();

        RecyclerView recyclerView = findViewById(R.id.recyclerViewUser);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ViewBinder<User> binder = (item, itemView) -> {
            TextView email = itemView.findViewById(R.id.emailTextView);
            TextView password = itemView.findViewById(R.id.passwordTextView);
            TextView role = itemView.findViewById(R.id.roleTextView);

            email.setText(item.getEmail());
            password.setText(item.getPassword());
            role.setText(item.getRole());

            itemView.setOnClickListener(v -> {
                emailEdit.setText(item.getEmail());
                passwordEdit.setText(item.getPassword());
                selectedId = item.getId();
                int position = findPositionByValue(item.getRole());
                if (position != -1) roleSpinner.setSelection(position);
            });
        };

        adapter = new RecyclerViewAdapter<>(users, binder, R.layout.user_card);
        recyclerView.setAdapter(adapter);

        findViewById(R.id.addBtn).setOnClickListener(v -> addUser());
        findViewById(R.id.updateBtn).setOnClickListener(v -> updateUser());
        findViewById(R.id.delBtn).setOnClickListener(v -> delUser());
        findViewById(R.id.prevBtn).setOnClickListener(v -> finish());

        loadData();
    }

    private void addUser() {
        String email = emailEdit.getText().toString();
        String password = passwordEdit.getText().toString();

        if (!validateForm(email, password)) return;

        User user = new User(email, password, selectedRole);

        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        String userId = auth.getCurrentUser() != null ? auth.getCurrentUser().getUid() : null;
                        if (userId == null) return;
                        user.setId(userId);

                        db.collection("users").document(userId).set(user)
                                .addOnSuccessListener(aVoid -> {
                                    users.add(user);
                                    clearInput();
                                    onSuccess();
                                })
                                .addOnFailureListener(this::onFailure);
                    } else {
                        onFailure(task.getException());
                    }
                });
    }

    private void updateUser() {
        if (selectedId.isEmpty() || selectedRole.isEmpty()) return;

        db.collection("users").document(selectedId)
                .update("role", selectedRole)
                .addOnSuccessListener(aVoid -> {
                    for (User user : users) {
                        if (user.getId().equals(selectedId)) {
                            user.setRole(selectedRole);
                            break;
                        }
                    }
                    clearInput();
                    onSuccess();
                })
                .addOnFailureListener(this::onFailure);
    }

    private void delUser() {
        if (selectedId.isEmpty()) return;

        db.collection("users").document(selectedId)
                .delete()
                .addOnSuccessListener(aVoid -> {
                    users.removeIf(user -> user.getId().equals(selectedId));
                    clearInput();
                    onSuccess();
                })
                .addOnFailureListener(this::onFailure);
    }

    private void spinnerInit() {
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(
                this, R.array.roles, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        roleSpinner.setAdapter(arrayAdapter);

        roleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedRole = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private boolean validateForm(String email, String password) {
        if (email.isEmpty() || password.isEmpty() || selectedRole.isEmpty()) {
            Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.length() < 6) {
            Toast.makeText(this, "Пароль менее 6 символов", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(this, "Неверный формат почты", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private int findPositionByValue(String value) {
        ArrayAdapter<?> stringAdapter = (ArrayAdapter<?>) roleSpinner.getAdapter();
        for (int i = 0; i < stringAdapter.getCount(); i++) {
            if (stringAdapter.getItem(i).toString().equals(value)) {
                return i;
            }
        }
        return -1;
    }

    private void loadData() {
        db.collection("users").get()
                .addOnSuccessListener(querySnapshot -> {
                    users.clear();
                    users.addAll(querySnapshot.toObjects(User.class));
                    adapter.notifyDataSetChanged();
                })
                .addOnFailureListener(this::onFailure);
    }

    private void clearInput() {
        emailEdit.setText("");
        passwordEdit.setText("");
        roleSpinner.setSelection(1);
        adapter.notifyDataSetChanged();
    }

    private void onSuccess() {
        Toast.makeText(this, "Операция прошла успешно", Toast.LENGTH_SHORT).show();
    }

    private void onFailure(@NonNull Exception ex) {
        Toast.makeText(this, "Ошибка: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
