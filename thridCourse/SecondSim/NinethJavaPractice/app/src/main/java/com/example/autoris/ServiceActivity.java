package com.example.autoris;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class ServiceActivity extends AppCompatActivity {
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private final List<Service> services = new ArrayList<>();
    private RecyclerViewAdapter<Service> adapter;
    private EditText nameServiceEdit;
    private String selectId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);

        nameServiceEdit = findViewById(R.id.nameEdit);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ViewBinder<Service> binder = (item, itemView) -> {
            TextView textView = itemView.findViewById(R.id.nameService);
            textView.setText(item.getName());
            itemView.setOnClickListener(v -> {
                selectId = item.getId();
                nameServiceEdit.setText(item.getName());
            });
        };

        adapter = new RecyclerViewAdapter<>(services, binder, R.layout.service_card);
        recyclerView.setAdapter(adapter);

        findViewById(R.id.addBtn).setOnClickListener(v -> addService());
        findViewById(R.id.updateBtn).setOnClickListener(v -> updateService());
        findViewById(R.id.delBtn).setOnClickListener(v -> delService());
        findViewById(R.id.prevBtn).setOnClickListener(v -> finish());
        loadData();
    }

    private void addService() {
        String name = nameServiceEdit.getText().toString();
        if (name.isEmpty()) return;

        Service service = new Service();
        service.setName(name);

        db.collection("service").add(service)
                .addOnSuccessListener(documentReference -> {
                    service.setId(documentReference.getId());
                    documentReference.update("id", documentReference.getId())
                            .addOnSuccessListener(aVoid -> {
                                services.add(service);
                                clearInput();
                                onSuccess();
                            })
                            .addOnFailureListener(this::onFailure);
                })
                .addOnFailureListener(e -> onFailure(e));
    }

    private void updateService() {
        if (selectId.isEmpty()) return;

        String name = nameServiceEdit.getText().toString();
        if (name.isEmpty()) return;

        db.collection("service").document(selectId)
                .update("name", name)
                .addOnSuccessListener(aVoid -> {
                    for (Service service : services) {
                        if (service.getId().equals(selectId)) {
                            service.setName(name);
                            break;
                        }
                    }
                    clearInput();
                    onSuccess();
                })
                .addOnFailureListener(this::onFailure);
    }

    private void delService() {
        if (selectId.isEmpty()) return;

        db.collection("service").document(selectId)
                .delete()
                .addOnSuccessListener(aVoid -> {
                    services.removeIf(service -> service.getId().equals(selectId));
                    clearInput();
                    onSuccess();
                })
                .addOnFailureListener(this::onFailure);
    }

    private void loadData() {
        db.collection("service").get()
                .addOnSuccessListener(querySnapshot -> {
                    services.addAll(querySnapshot.toObjects(Service.class));
                    adapter.notifyDataSetChanged();
                })
                .addOnFailureListener(this::onFailure);
    }

    private void clearInput() {
        nameServiceEdit.setText("");
        adapter.notifyDataSetChanged();
    }

    private void onSuccess() {
        Toast.makeText(this, "Операция прошла успешно", Toast.LENGTH_SHORT).show();
    }

    private void onFailure(@NonNull Exception ex) {
        Toast.makeText(this, "Ошибка: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
